# Escritura Analogica con RGB

En este repositorio encontraras información sobre como usar y controlar un led RGB con Arduino a través de PWM.

## Materiales

* **Una placa Arduino**
* **Un Led RGB**
* **3 Resistencias de 220Ω**
* **Un Protoboard**

## Uso

```
En la carpeta diagrams encontraras la conexion del circuito.
```

```
En la carpeta src encontraras el codigo fuente de ejemplo.
```

Deberas verificar cual es la pata comun de tu led RGB, hay 2 opciones, catodo comun (-) o anodo comun (+), una vez identificada usa el diagrama que mas te convenga, por otro lado en el codigo de arduino deberas hacer una pequeña modificación en caso de que sea uno u otro.


## Autor

* **Edermar Dominguez** - [Ederdoski](https://gitlab.com/Ederdoski/about)

## License

This code is open-sourced software licensed under the [MIT license.](https://opensource.org/licenses/MIT)

