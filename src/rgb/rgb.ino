int pinRed   = 11;     // Pin 11 a R
int pinGreen = 10;   // Pin 10 a G
int pinBlue  = 9;     // Pin 9 a B
int balance = 255;

/*
 * Este metodo esta adaptado para un Led RGB con Anodo(+) comun,
 *en caso de tener un RGB con Catodo(-) comun, deberas eliminar la variable
 *balance
*/

void color(int R, int G, int B)
{     
    analogWrite(pinRed , balance - R) ;    
    analogWrite(pinGreen, balance - G) ;  
    analogWrite(pinBlue, balance - B) ;
}
   
void setup() 
{
    pinMode(pinRed, OUTPUT);
    pinMode(pinGreen, OUTPUT);
    pinMode(pinBlue, OUTPUT);
}
 
void loop() {
  
  color(0,0,255);
  
}
